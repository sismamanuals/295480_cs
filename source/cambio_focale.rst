﻿#############
CAMBIO FOCALE
#############
Un aspetto importante riguarda il cambio di focale. Se la macchina è equipaggiata con più focali è possibile cambiarla  per differenti esigenze.

Per fare il cambio è necessario rimuovere la focale montata ed avvitare la nuova focale sulla testa laser.

************************************
CAMBIO IMPOSTAZIONE FOCALE SU PRISMA
************************************
Una volta montata la nuova focale è necessario comunicare a PRISMA la modifica affinché possa essere utilizzata la configurazione adeguata. Per questo:

* Aprire la finestra :guilabel:`Strumenti > Hardware` e selezionare l'impostazione :guilabel:`File`;
* Selezionare dalla lista il settaggio relativo alla focale montata;
* Premere :kbd:`APPLICA` o :kbd:`OK` e chiudere la finestra;
* Eseguire un Homing dell'asse Z per accertarsi che le nuove impostazioni vengano caricate.

.. NOTE::

   |notice|
   Qualora la focale montata non abbia il suo corrispondente file di impostazione, contattare l'assistenza tecnica Sisma per l'inserimento del file mancante.

.. _cambio_focale_impostazioni:
.. figure:: _static/cambio_focale_impostazioni.png
   :width: 14 cm
   :align: center

   Impostazioni *Cambio focale*
