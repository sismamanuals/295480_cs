﻿.. _test_PFS:

########
TEST PFS
########
.. DANGER::

   |danger| **Pericolo di danni a cose e/o persone**

   Questo test permette di andare ad eseguire una marcatura sul materiale per identificare i parametri più idonei al materiale stesso.

   La compilazione dei parametri del test richiede una buona conoscenza delle modalità di utilizzo del programma e familiarità con i parametri macchina.

   L'impostazione dei parametri per la marcatura è riservata ad un utente esperto.

.. _PFS_test:
.. figure:: _static/PFS_test.png
   :width: 14 cm
   :align: center

   Finestra *PSF TEST*

***************
ESECUZIONE TEST
***************
Il test eseguirà una serie di marcature incrociando i range di *Potenza* - *Frequenza* - *Velocità* che vogliamo verificare.

Per il test si procede decidendo quale dei 3 parametri mantenere fisso. In funzione della scelta si andranno ad impostare i limiti minimo e massimo degli altri due parametri.

Ad esempio (vedere :numref:`PFS_test`), se si desidera lasciare fissa la *Velocità* basterà selezionare *Potenza* - *Frequenza*.

Fissato questo parametro seguire le seguenti istruzioni di compilazione del test:

#. Impostare il range (Min-Max) dei valori da testare ed il valore da assegnare al parametro fisso scelto;
#. Verificare che:

   * la :kbd:`Forma d'Onda` sia quella che desideriamo testare;
   * la :kbd:`Spaziatura Riempimento` sia corretta per il test;
   * lo :kbd:`Zoom [%]` permetta di avere una dimensione del test adeguata all'area da marcare.

#. Selezionare tra le ulteriori opzioni presenti quelle necessarie al test:

   * :kbd:`Usa Livello Corrente` *inserire descrizione*;
   * :kbd:`Riempimento bidirezionale` *inserire descrizione*;
   * :kbd:`Linea Sottile` *inserire descrizione*;
   * :kbd:`Non muovere testa` *inserire descrizione*.

#. Premere :kbd:`AVVIA ROSSO`;
#. Posizionare il materiale all'interno della camera, centrando il rosso sull'area interessata alla marcatura;
#. Chiudere il portellone del laser;
#. Premere :kbd:`AVVIA LASER`
#. :kbd:`Aria` *inserire descrizione*;
#. :kbd:`STOP` permette di arrestare il test.

**************
RISULTATO TEST
**************
Una volta eseguito il test verificare se uno dei quadratini marcati soddisfa le esigenze di marcatura.

In caso di risposta affermativa, per poter utilizzare i parametri trovati:

#. Chiudere il :guilabel:`PFS TEST`;
#. Aprire un File Livelli nuovo;
#. Copiare i valori indicati nella piastrina del test nel colore del Livello scelto.
