﻿##############
TEST PARAMETRI
##############
È una ricerca guidata dei parametri per la marcatura. Selezionare da :guilabel:`Strumenti > Test parametri`

Questo test permette di andare ad eseguire una marcatura guidata sul materiale per identificare i parametri più idonei al materiale stesso.

È un test rivolto ad un utente di livello base (entry level). Nel sistema sono presenti altri test ma sono rivolti ad utenti più esperti che conoscano già le modalità di utilizzo del programma e abbiano familiarità con i parametri macchina.

*************************
RICERCA GUIDATA PARAMETRI
*************************
Seguire passo passo le informazioni richieste dal percorso guidato.

Focale
======

.. _test_parametri_focale:
.. figure:: _static/test_parametri_focale.png
   :width: 14 cm
   :align: center

   Finestra *Focale*

.. WARNING::

   |warning| **Pericolo di danni a cose e persone**

   Se si usa una focale diversa dalla focale 160 mm è opportuno indicare nella fase di selezione del materiale la voce GENERICO.

Premere :kbd:`AVANTI`.

Posizione
=========

.. _test_parametri_posizione:
.. figure:: _static/test_parametri_posizione.png
   :width: 14 cm
   :align: center

   Finestra *Posizione*

Impostare la posizione con uno dei seguenti metodi:

#. Selezionare :kbd:`HOMING` per muovere la testa nella posizione di origine; *oppure*
#. Selezionare :kbd:`INSERISCI QUOTE ATTUALI` per completare i campi sottostanti con la posizione attuale; *oppure*
#. Inserire le quote manualmente dei campi :kbd:`X`, :kbd:`Y` e :kbd:`Z`.

* Premere :kbd:`GO` per portare la testa nella posizione desiderata.
* Premere :kbd:`AVANTI`.

Material and Process
====================

.. _test_parametri_material_and_process:
.. figure:: _static/test_parametri_material_and_process.png
   :width: 14 cm
   :align: center

   Finestra *Material and Process*

* Selezionare il tipo di materiale che si intente testare. Se il materiale non è presente nella lista o la focale è diversa da f=160mm si consiglia di selezionare GENERICO;
* Selezionare uno dei tipi di lavorazione proposta per ogni materiale;
* Premere :kbd:`AVANTI`.

Test
====

.. _test_parametri_test:
.. figure:: _static/test_parametri_test.png
   :width: 14 cm
   :align: center

   Finestra *Test*

Nella finestra :guilabel:`TEST` si possono impostare:

* :kbd:`ZOOM` (rappresenta le dimensioni massime del test, si cambia attraverso il cursore);
* :kbd:`STAMPA VALORI` (stampa i tempi vicino alle marcature eseguite);
* :kbd:`MODALITÀ` (permette di scegliere se le marcature si riferiranno ad un riempimento - :guilabel:`Area` - oppure ad un bordo - :guilabel:`Testo`);
* :kbd:`FORMATO` (numero di prove, a scelta tra :guilabel:`4x4`, :guilabel:`8x4` oppure personalizzando la matrice)


* Premere :kbd:`AVVIA ROSSO` per proiettare l'ingombro massimo all'interno della camera di lavoro. Tramite il rosso è possibile posizionare il pezzo in posizione desiderata.
* Chiudere la porta del laser.
* Premere :kbd:`AVVIA LASER` per eseguire il test.

Avanzato
========

.. _test_parametri_avanzato:
.. figure:: _static/test_parametri_avanzato.png
   :width: 14 cm
   :align: center

   Finestra *Avanzato*

*Inserire descrizione**

**************
RISULTATO TEST
**************
Se uno dei quadratini marcati soddisfa le nostre esigenze è sufficiente:

#. Cliccare sul quadrato corrispondente (punto **1** di :numref:`test_parametri_test_salvataggio1`);
#. Premere :kbd:`SALVA` (punto 2 di :numref:`test_parametri_test_salvataggio1`).

.. _test_parametri_test_salvataggio1:
.. figure:: _static/test_parametri_test_salvataggio1.png
   :width: 14 cm
   :align: center

   Finestra *Test* - Salvataggio impostazioni


Si aprirà la finestra in :numref:`test_parametri_test_salvataggio2` dove è necessario:
#. Scegliere il file di configurazione;
#. Scegliere il colore del livello a cui vogliamo abbinare i parametri trovati.
#. Chiudere la finestra;
#. Richiamare il file di configurazione modificato
#. Eseguire la marcatura del file con i parametri identificati.

.. _test_parametri_test_salvataggio2:
.. figure:: _static/test_parametri_test_salvataggio2.png
   :width: 10 cm
   :align: center

   Finestra *Test* - Salvataggio impostazioni su livello

.. NOTE::

   |notice| Se il risultato atteso è compreso tra un gruppo di parametri, è possibile selezionare i quadrati relativi ai parametri scelti ed eseguire un'ulteriore marcatura nel range indicato. Nella :guilabel:`LISTA TEST` apparirà un nuovo test (vedere :numref:`test_parametri_test_multi`).

Al termine del secondo test si può andare ad identificare il quadratino relativo al parametro scelto e salvare il parametro scegliendo il nome della configurazione Livelli e il colore a cui abbinarlo.

.. _test_parametri_test_multi:
.. figure:: _static/test_parametri_test_multi.png
   :width: 14 cm
   :align: center

   Finestra *Test* - Test ulteriori
