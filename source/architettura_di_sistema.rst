#######################
ARCHITETTURA DI SISTEMA
#######################
******************
STRUTTURA SOFTWARE
******************
PRISMA funge da interfaccia grafica per l'applicativo server SLC. Quest'ultimo permette il controllo della macchina, anche da remoto. SLC, oltre alla gestione di tutti i componenti hardware, trasmette le informazioni a PRISMA.
Per questo motivo PRISMA e SLC dialogano continuamente tra loro.

****************
CONTROLLO REMOTO
****************
L'architettura sopra descritta permette di controllare da remoto una stazione laser di marcatura. 

PRISMA, supportando questo tipo di configurazione, permette di controllare più stazioni di marcatura simultaneamente. È possibile passare da una macchina ad un'altra semplicemente agendo su un selettore.
Sfruttando il protocollo di comunicazione che sta alla base del dialogo tra SLC Server e PRISMA, si possono anche realizzare nuove interfacce di controllo ad-hoc secondo le esigenze del cliente o della lavorazione.
Per illustrare meglio le potenzialità del sistema si possono identificare alcuni scenari di funzionamento possibili grazie a questa nuova architettura.

Configurazione Base
===================
Si può definire configurazione *base* la modalità più semplice di funzionamento. In questo caso SLC e PRISMA risiedono nello stesso computer.

Questa è la situazione più diffusa e normalmente utilizzata per il controllo della macchina.

Configurazione unico Client - molti Server
==========================================
Più sistemi laser con computer interconnessi tra loro in un'unica rete logica determinano una configurazione *unico Client - molti Server*. Ogni marcatore è autonomo e completamente indipendente dagli altri. A questa rete viene aggiunto un altro terminale, che potrebbe anche non avere una stazione di marcatura direttamente connessa.

Uno scenario simile potrebbe essere utile in una situazione dove si richiede un computer unico per il controllo di più postazioni di marcatura.
Dal computer possono essere eseguite tutte le operazioni come se si stesse lavorando in locale.

Configurazione molti Client - unico Server
==========================================
Un'unica postazione di marcatura e più computer connessi sulla rete che possono interagire con essa determinano una configurazione *molti Client - unico Server*. Il sistema di marcatura, in questo caso, può essere paragonato ad una “stampante” a tutti gli effetti.
