#############
INSTALLAZIONE
#############
********************
REQUISITI DI SISTEMA
********************
PRISMA è stato sviluppato in ambiente Microsoft Visual Studio ed è pienamente compatibile con i sistemi operativi Windows.
Per funzionare è richiesto che nel computer sia installato il componente **Microsoft Framework 4.7 Full Edition**, scaricabile gratuitamente dal sito Microsoft.
PRISMA offre una nuova interfaccia grafica e sfrutta come motore di marcatura il software già noto Sisma Laser Controller (SLC).
I requisiti necessari per il funzionamento della macchina sono:

* Sistema Operativo Microsoft Windows 10
* RAM 1 GB
* Porte USB/Seriali a seconda del modello di macchina

PRISMA si auto-configura leggendo le informazioni da SLC.

Tutti i driver e le librerie necessarie per il funzionamento della macchina sono legate all'hardware, che differisce da un sistema laser ad un altro.

Per altri eventuali requisiti e limitazioni si rimanda alle schede tecniche dei prodotti installati.

**************************
PROCEDURA DI INSTALLAZIONE
**************************
L'installazione viene eseguita tramite l'apposito file **PRISMASetup.exe**. Eseguendo questo file vengono fatti tutti i controlli per valutare se il computer che si sta utilizzando è compatibile e viene avviata la procedura si installazione.

****************************
PRIMO AVVIO E CONFIGURAZIONE
****************************
Dopo l'installazione PRISMA è pronto per il primo avvio. Questa fase è particolarmente importante perché si vanno ad acquisire i parametri di configurazione della macchina collegata. Avviando PRISMA comparirà la finestra in :numref:`impostazioni_applicazione_sistemi_laser`, dove sono mostrati tutti i sistemi laser collegati a PRISMA.

Al primo avvio l'elenco è vuoto, salvo per una macchina dimostrativa che si chiama *Generic Laser*.

.. _impostazioni_applicazione_sistemi_laser:
.. figure:: _static/impostazioni_applicazione_sistemi_laser.png
   :width: 14 cm
   :align: center

   Impostazioni del programma


Per procedere con l'importazione dei dati della macchina:

#. Premere il pulsante :kbd:`+`;
#. Confermare con :kbd:`OK`. La procedura di importazione dura qualche decina di secondi e, al termine, si ritorna nella finestra di :guilabel:`Impostazioni`.
#. Confermare con :kbd:`OK` per salvare i dati.

PRISMA è pronto per il primo utilizzo.

Per utilizzare PRISMA per scopi dimostrativi non è necessario configurare una macchina, si può utilizzare quella già presente.

*Per non mostrare continuamente in apertura del software la finestra di configurazione macchina si può utilizzare l'opzione in alto a destra della finestra stessa per disabilitarne la visualizzazione all'avvio.* *Da spiegare meglio*
