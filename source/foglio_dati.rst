﻿###########
FOGLIO DATI
###########
Un foglio dati rappresenta un tipo di visualizzazione semplice in cui i dati sono disposti in righe e colonne.
Per poter utilizzare i suoi dati per la marcatura è necessario che il foglio dati sia salvato in formato *csv* o *txt*.

************
INTRODUZIONE
************
Per poter utilizzare un Foglio dati è necessario:

* Selezionare, nella finestra a tendina, :guilabel:`Strumenti > Impostazioni > Applicazione` e aprire la finestra di gestione delle :guilabel:`Interfaccia`;
* Attivare la voce :kbd:`Contatori e Foglio Dati`
* Premere sull'icona Foglio Dati in baso a sinistra del programma.

.. _foglio_dati_icona_datasheet:
.. figure:: _static/foglio_dati_icona_datasheet.png
   :width: 10 cm
   :align: center

   Aggiungi *Foglio di Lavoro*

*******************
COMANDI FOGLIO DATI
*******************

.. _foglio_dati_barra_comandi:
.. figure:: _static/foglio_dati_barra_comandi.png
   :width: 14 cm
   :align: center

   Barra comandi *Foglio di Lavoro*

Di seguito vengo descritti i comandi che di trovano nella finestra Foglio Dati.

* :kbd:`Abilita` (attiva l'utilizzo del Foglio Dati);
* :kbd:`Carica Foglio Dati` (apre la finestra di importazione del file csv o txt);
* :kbd:`Ricarica` (ricarica il file importato);
* :kbd:`Autocaricamento` (ricarica il file ad ogni modifica del file origine);
* :kbd:`Autocaricamento setup` (definisce il setup per l'importazione del file);
* :kbd:`Pulisci Foglio Dati` (rimuove dal foglio tutti dati);
* :kbd:`Rimuovi dopo marcatura` (dopo aver marcato la riga, la elimina del foglio dati);
* :kbd:`Riparti dopo la fine` (al termine dell'utilizzo di tutti i dati del foglio il sistema riparte a marcare dall'inizio);
* :kbd:`Incrementa ad ogni marcatura` (se i dati sono utilizzati su più piani, aggiorna il valore dei dati al passaggio del piano successivo);
* :kbd:`Incrementa ad ogni ciclo` (se i dati sono utilizzati su più piani, aggiorna il valore dei dati al passaggio del ciclo successivo di marcatura);
* :kbd:`Taglia/Copia/Incolla` (permette di eseguire queste operazioni sulle righe del foglio);
* :kbd:`Aggiungi/Cancella riga` (permette di eseguire queste operazioni sulla riga selezionata);
* :kbd:`Sposta su/Sposta giù` (permette si traslare la riga selezionata verso l'alto o verso il basso);
* :kbd:`Inizio/Indietro/Valore/Avanti/Fine` (permette si spostarsi nelle righe del foglio);
* :kbd:`Report` (crea un report relativo alle righe marcate, dopo aver creato il file nel PC o in un disco di rete);
* :kbd:`Nome report` (mostra il nome del file in cui si sta scrivendo il report).

*************************************************
ASSEGNAZIONE DATI DEL FOGLIO A VARIABILI NEL FILE
*************************************************
Indicare nel file di marcatura i punti in cui i dati del foglio vanno inseriti.

Per fare questo bisogna scrivere nel file un Testo Lineare che sia nel formato **#[DATO]#**, dove al posto della voce **DATO** venga inserito il nome della colonna dei dati da inserire (esempio: **#[COLUMN1]#**). Ovviamente il testo può poi avere il font, la dimensione e la posizione desiderato all'interno del file grafico.

*********************
MARCATURA FOGLIO DATI
*********************
Una volta assegnati i dati del Foglio ai relativi campi del file grafico è possibile passare alla marcatura del file.

Per marcare il file si ricorda che è necessario:

* Settare :kbd:`altezza Z[mm]` del punto di marcatura nella finestra Struttura Progetto e premere :kbd:`GO`.
* Selezionare i parametri di marcatura nella finestra Livelli.
* Premere :kbd:`ROSSO` per centrare l'area di marcatura sul pezzo.
* Chiudere lo sportello.
* Premere :kbd:`START` per eseguire la marcatura.
