﻿############
MENÙ PRISMA
############
Nella parte superiore della finestra dell'applicazione si trovano menu e barra degli strumenti.

Il menù contiene il tutti i comandi per la gestione del progetto.

Si trovano anche i collegamenti per accedere alle finestre di impostazione per applicazione, hardware e procedure di lavorazione.

.. _menù_prisma:
.. figure:: _static/menù_prisma.png
   :width: 14 cm
   :align: center

   Menù PRISMA

****
FILE
****
:kbd:`File` è un menù a tendina che permette di selezionare le principali azioni che si posso eseguire sul progetto:

* :kbd:`Nuovo` crea un progetto nuovo;
* :kbd:`Apri` apre un progetto già salvato, permettendo di selezionarlo da una cartella del PC o di rete;
* :kbd:`Chiudi` chiude il progetto attualmente in uso;
* :kbd:`Salva` salva il progetto attivo;
* :kbd:`Salva con nome` salva con un nome diverso il progetto attivo;
* :kbd:`Importa da SLC` importa un file di configurazione **XML** precedentemente creato in SLC con relativi file, livelli, focale e posizione;
* :kbd:`Esci` esce dal programma PRISMA, chiudendo tutti i progetti aperti.

********
MODIFICA
********
* :kbd:`Annulla` cancella l'ultima operazione eseguita sui file presenti nel progetto attivo;
* :kbd:`Ripeti` ripete l'ultima operazione annullata.

********
PROGETTO
********
* :kbd:`Aggiungi File` permette di aggiungere un file esistente nel progetto attivo;
* :kbd:`Impostazioni Progetto` apre una finestra per impostare le principali proprietà del progetto;

   * :kbd:`Generico` mostra il nome del progetto;
   * :kbd:`Assi e Griglia` definisce:
      * :kbd:`Unità di misura` (mm o inches);
      * :kbd:`Posizione Zero` (posizioni predefinite con griglia o posizione personalizzata);
      * Caratteristiche degli assi;
      * :kbd:`Griglia` (per avere la presenza della griglia a schermo);
      * :kbd:`Precisione Posizione` e :kbd:`Precisione Rotazione` (definisce la precisione dei parametri).

.. _impostazioni_progetto:
.. figure:: _static/impostazioni_progetto.png
   :width: 14 cm
   :align: center

   Finestra di impostazioni progetto

*********
STRUMENTI
*********
Di seguito si fornisce una breve descrizione. Descrizioni con maggior dettaglio verranno fornite per specifiche funzionalità.

* :kbd:`Test Parametri` (permette un ricerca guidata dei parametri);
* :kbd:`Hardware` (contiene le impostazioni hardware del laser - i suoi contenuti possono venir modificati solo da un tecnico Sisma);
* :kbd:`Impostazioni` (contiene la finestra :guilabel:`Applicazione` con le relative sotto-finestre per la gestione di diversi parametri del programma. Vedere :ref:`impostazioni_applicazione`);
* :kbd:`Laser Tests` (contiene un serie di test da eseguirsi con la macchina, tutti questi test sono riservati al personale tecnico Sisma, tranne :ref:`test_PFS`, :ref:`test_focus` e :ref:`test_3D`);
* :kbd:`Convertitore DXF DWG -> SVG` (permette di selezionare un file **DXF DWG** e convertirlo in **SVG**, salvandolo nella stessa cartella del file origine. Non viene importato il file nel progetto).
* :kbd:`Convertitore PLT -> SVG` (permette di selezionare un file **PLT** e convertirlo in **SVG**, salvandolo nella stessa cartella del file origine. Non viene importato il file nel progetto).

*****
DEBUG
*****
* :kbd:`Chiudi SLC` (*da descrivere*);
* :kbd:`Reset SLC` (*da descrivere*);
* :kbd:`Nascondi SLC` (*da descrivere*);
* :kbd:`Mostra SLC` (*da descrivere*).

*****
AIUTO
*****
* :kbd:`Informazioni su` (apre la schermata dove è possibile vedere la versione installata del programma PRISMA).
